package com.rekla.jenkins_git.unbreakable_build;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnbreakableBuildApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnbreakableBuildApplication.class, args);
    }

}
